package com.koraly.koralyapi.documents;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Document("persons")
@Data
public class Person {
    @Id
    private String oid;

    private UUID idd;

    private String firstName;
    private String lastName;
    private String email;
    private Gender gender;

    private LocalDate birthDate;

    private List<Address> addresses;
}
