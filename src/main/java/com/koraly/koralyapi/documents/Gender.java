package com.koraly.koralyapi.documents;

public enum Gender {
    MALE, FEMALE
}
