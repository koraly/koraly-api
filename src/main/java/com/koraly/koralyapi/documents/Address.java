package com.koraly.koralyapi.documents;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("addresses")
@Data
public class Address {
}
