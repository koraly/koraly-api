package com.koraly.koralyapi.controllers;

import com.koraly.koralyapi.model.PersonDTO;
import com.koraly.koralyapi.repositories.PersonRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.UUID;

@RestController
public class PersonController implements PersonsApi {

    private final ModelMapper modelMapper;
    private final PersonRepository personRepository;

    public PersonController(ModelMapper modelMapper, PersonRepository personRepository) {
        this.modelMapper = modelMapper;
        this.personRepository = personRepository;
    }

    @Override
    public Mono<ResponseEntity<Flux<PersonDTO>>> findPersons(Optional<UUID> glueCode, ServerWebExchange exchange) {
        Flux<PersonDTO> persons = personRepository.findAll()
                .map(person -> modelMapper.map(person, PersonDTO.class));
        return Mono.just(ResponseEntity.ok(persons));
    }

    @Override
    public Mono<ResponseEntity<PersonDTO>> getPerson(UUID personID, Optional<UUID> glueCode, ServerWebExchange exchange) {
        return personRepository.findByIdd(personID.toString())
                .map(person -> modelMapper.map(person, PersonDTO.class))
                .map(ResponseEntity::ok);
    }
}
