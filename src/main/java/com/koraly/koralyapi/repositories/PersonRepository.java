package com.koraly.koralyapi.repositories;

import com.koraly.koralyapi.documents.Person;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface PersonRepository extends ReactiveCrudRepository<Person, Long> {

    Flux<Person> findAllByFirstName(String name);

    Mono<Person> findByIdd(String idd);
}