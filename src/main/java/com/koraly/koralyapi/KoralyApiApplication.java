package com.koraly.koralyapi;

import com.koraly.koralyapi.documents.Person;
import com.koraly.koralyapi.model.PersonDTO;
import org.modelmapper.PropertyMap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.modelmapper.ModelMapper;

@SpringBootApplication
public class KoralyApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KoralyApiApplication.class, args);
	}



	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		PropertyMap<Person, PersonDTO> documentMap = new PropertyMap<>() {
			protected void configure() {
				map(source.getIdd(), destination.getId());
			}
		};

		modelMapper.addMappings(documentMap);
		return modelMapper;
	}
}
